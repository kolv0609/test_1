
package cz.vse.kolv.logika;

/*******************************************************************************
 * Třída HerniPlan - představuje mapu hry.
 * 
 * Třída obsahuje základní prvky, které tvoří hru. Vytváří veškeré místnosti a veškeré věci.
 * Dále jednotlivým místnostem přiřazuje možné východy a do jednotlivých místností
 * vkládá nové věci.
 * 
 * 
 *
 * @author Vít Kollarczyk
 * @version 1.0
 */
public class HerniPlan
{
    private Batoh batoh;
    private Mistnost aktualniMistnost;
    private Vec jidlo;   
    
    private Mistnost vyherniMistnost;
    
    /**
     *Konstruktor pro vytvořená herního plánu.
     */
    public HerniPlan()
    {
        vytvorMistnosti();
        batoh = new Batoh();        
        
    }
    
    /**
     * Metoda vytváří jednotlivé místnosti a přiřazuje jim i možné východy. Dále do místností 
     * vkládá jednotlivé věci, které buď lze či nelze sebrat.
     * Výchozím prostorem, kde hra začíná je Předsíň. Jídlem, které je potřeba sníst k vyhrání hry je bonbon.
     */
    private void vytvorMistnosti()
    {
        Mistnost predsin = new Mistnost("Předsíň",", vstupní místnost do domu");
        Mistnost chodba = new Mistnost("Chodba",", ze které se vstupuje do dalších místností");
        Mistnost detsky_pokoj = new Mistnost("Dětský_pokoj",", ve které se nachází plyšový medvídek.");
        Mistnost loznice = new Mistnost("Ložnice",", místnost pro rodiče, kde se ukrývají některé předměty..");
        Mistnost obyvaci_pokoj = new Mistnost("Obývací_pokoj",", místnost, kde se odpočívá u televize");
        Mistnost koupelna = new Mistnost("Koupelna",", místnost, kde se nachází hygienické potřeby.");
        Mistnost pracovna = new Mistnost("Pracovna",", ve které se mimo jiné nachází trezor.");
        Mistnost kuchyn = new Mistnost("Kuchyň",", v této místnosti se nachází kuchyňské potřeby");
        
        
        Mistnost vyhra = new Mistnost("Vyhra",", toto je výherní místnost, kam se lze dostat pouze skrze průchod.");
            
            
        
            predsin.setVychod(chodba);
        
            chodba.setVychod(predsin);
            chodba.setVychod(detsky_pokoj);
            chodba.setVychod(loznice);
            chodba.setVychod(obyvaci_pokoj);
            chodba.setVychod(koupelna);
            
            detsky_pokoj.setVychod(chodba);
            koupelna.setVychod(chodba);
            loznice.setVychod(chodba);
        
            kuchyn.setVychod(obyvaci_pokoj);
            pracovna.setVychod(obyvaci_pokoj);
        
            obyvaci_pokoj.setVychod(pracovna);
            obyvaci_pokoj.setVychod(kuchyn);
            obyvaci_pokoj.setVychod(chodba);
        
        Vec boty = new Vec("boty",false);
        predsin.pridejVec(boty);
        Vec telefon = new Vec("telefon",true);
        chodba.pridejVec(telefon);
        Vec parfem = new Vec("parfém",true);
        koupelna.pridejVec(parfem);
        Vec klic = new Vec("klíč",true);
        loznice.pridejVec(klic);
        Vec televize = new Vec("televize",false);
        obyvaci_pokoj.pridejVec(televize);
        Vec nuz = new Vec("nuz",true);
        kuchyn.pridejVec(nuz);
        Vec trezor = new Vec("trezor",false);
        pracovna.pridejVec(trezor);
        trezor.setLzeOdemknout(true);
        Vec hodinky = new Vec("hodinky",true);
        trezor.vlozDovnitr(hodinky);
        Vec medvidek = (new Vec("medvidek",false));
        detsky_pokoj.pridejVec(medvidek);
        medvidek.setLzeRozriznout(true);
        Vec bonbon = new Vec("bonbon",true);
        medvidek.vlozDovnitr(bonbon);
        bonbon.setLzeSnist(true);
          
        Vec truhla = new Vec("truhla",false);
        truhla.setLzeOtevrit(true);
        chodba.pridejVec(truhla);
        Vec klicek = new Vec("klicek",true);
        truhla.vlozDovnitr(klicek);
        
        Vec portal = new Vec("portal", false);
        loznice.pridejVec(portal);
        portal.setLzeProjit(true);
        
        aktualniMistnost = predsin;
        jidlo = bonbon;      
        vyherniMistnost = vyhra;
    }    
    
    /**
     * Vrací aktuální místnost, ve které se hráč momentálně nachází.
     * 
     * @return aktuální místnost
     */
    public Mistnost getAktualniMistnost()
    {
        return aktualniMistnost;
    }
    
    /**
     * Metoda nastaví aktuální prostor. Např. při průchodu mezi jednotlivými místnostmi.
     * 
     * @param mistnost - nová aktuální místnost.
     */
    public void setAktualniMistnost(Mistnost mistnost)
    {
        aktualniMistnost = mistnost;
    }
    
    /**
     * Metoda vrací batoh, do kterého se během hry vkládají věci.
     * 
     * @return batoh
     */
    public Batoh getBatoh()
    {
        return this.batoh;
    }
    
    /**
     * Metoda ověřuje, zda došlo ke splnění všech podmínek pro vítězství - ukončení hry, 
     * tedy zda-li byl snězen bonbon a dále se v batohu nacházejí všechny předměty, které patří mezi dárky.
     */
    /*
    public boolean vyhra()
    {
        return (batoh.obsahujeVec("telefon")&&batoh.obsahujeVec("parfém")&&batoh.obsahujeVec("hodinky")
        &&jidlo.jeSneden());
    }
    */
    public boolean vyhra()
    {
        if(batoh.obsahujeVec("telefon")&&batoh.obsahujeVec("parfém")&&batoh.obsahujeVec("hodinky")
        &&jidlo.jeSneden())
        {
            return true;
        }
        
        if(getAktualniMistnost().equals(vyherniMistnost))
        {
            return true;
        }
        return false;
    }
    
}
