
package cz.vse.kolv.logika;

import java.util.*;
import java.util.stream.Collectors;

/*******************************************************************************
 * Třída Mistnost - popisuje jednotlivé místnosti ve hře.
 * 
 * Mistnost jsou jednotlivé pokoje, do kterých může hráč během hraní vstupovat.
 * Každý pokoj má sousední místnost a do každého pokoje lze vstoupit.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class Mistnost
{
    private String nazev;
    private String popis;
    private Set<Mistnost> vychody;
    private Map<String, Vec> veci;

    /**
     * Vytvoření místnosti s parametry název a popis, např.: "Předsíň",", vstupní místnost do domu".
     * 
     * @param nazev - název Místnosti např. Předsíň. Název je bez mezer.
     * @param popis - popis místnosti např. ", vstupní místnost do domu"
     */
    public Mistnost(String nazev, String popis)
    {
        this.nazev = nazev;
        this.popis = popis;
        vychody = new HashSet<>();
        veci = new HashMap<>();
    }
    
    /**
     * nastaví možný východ z místnosti. Z místností lze vcházet pouze do těch pokojů, 
     * které jsou aktuálně v seznamu dostupných východů. Tento seznam se mění při průchodu
     * jednotlivými místnostmi.
     */
    public void setVychod(Mistnost vedlejsiMistnost)
    {
        vychody.add(vedlejsiMistnost);
    }
    
    /**
     * Metoda porovnává dva různé pokoje. Dvě místnosti jsou shodné v případě, že mají shodný název.
     * Metoda je důležitá pro správné fungování nastavení dostupných východů pro jednotlivé místnosti.
     * 
     * @param o object, porovnává se s aktuálním.
     * @return true - pro dvě místnosti se stejným názvem, v jiném případě vrátí false
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
            return true;
        }
        
        if(!(o instanceof Mistnost))
        {
            return false;
        }
        
        Mistnost dalsi = (Mistnost) o;
        
        return (java.util.Objects.equals(this.nazev, dalsi.nazev));
    }
    
    /**
     * metoda hashCode vrací identifikátor instance ve formě čísla. Používá se pro
     * optimalizaci ukladani v dynamickych datových strukturách. Při překrytí metody equals
     * je nutné překrýt i metodu hashCode.
     */
    @Override
    public int hashCode()
    {
        int vysledek = 3;
        int hashNazev = java.util.Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazev;
        return vysledek;
    }
    
    /**
     * Vrací název místnosti
     * 
     * @return nazev místnosti
     */
    public String getNazev()
    {
        return nazev;
    }
    
    /**
     * Vrací řetězec, který popisuje danou místnost. Veškeré možné východy a věci, které se v místnosti nacházejí.
     * 
     * @return dlouhyPopis místnosti
     */
    public String dlouhyPopis()
    {
        return "Právě se nacházíš v místnosti " +  nazev + popis + ".\n" + 
        mozneVychody() + ".\n" + seznamVeciVMistnosti();
        
    }
    
    /**
     * Vrací seznam dostupných východů z místnosti.
     * 
     * @return mozne vychody - názvy sousedních pokojů
     */
    private String mozneVychody()
    {
        String navratovyText = "východy: ";
        for(Mistnost vedlejsi : vychody)
        {
            navratovyText += " " + vedlejsi.getNazev();
        }
        return navratovyText;
    }
    
   /**
    * Ověřuje, zda-li se v místnosti nachází nějaká věc, popřípadě, jestli uvnitř nějaké věci není jiná věc.
    * 
    * @param nazevVeci - název věci, která se vyhledává
    * @return true pokud se věc v místnosti/předmětu najde jinak false
    */
    public boolean obsahujeVec(String nazevVeci)
    {
        if(najdi(nazevVeci) == null)
        {
            for(Vec neco : veci.values())
            {
                if(neco.obsahuje(nazevVeci))
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            return true;
        }
    }
    
    /**
     * Odebírá věci z místnosti popřípadě věci, které jsou uvnitř jiných věcí v místnosti.
     * 
     * @param nazev - nazev věci, která se odebírá
     * @return vybrana věc, která se odebere
     */   
    public Vec odeberVec(String nazev)
    {
        Vec vybrana = najdi(nazev);
        if(vybrana != null && vybrana.lzeSebrat())
        {
            veci.remove(vybrana.getNazev(),vybrana);
        }
        else
        {
            for(Vec neco : veci.values())
            {
                vybrana = neco.vytahni(nazev);
                if(vybrana != null)
                {
                    break;
                }
            }
        }
        return vybrana;
    }
    
   /**
    * Ověřuje, zda-li se zadaná věc nachází v místnosti.
    * 
    * @param hledanaVec - věc, kterou v místnosti hledám
    * @return nalezeno - pokud nalezne, vrátí danou věc, jinak null
    */
    private Vec najdi(String hledanaVec)
    {
        Vec nalezeno = null;
        for(Vec neco : veci.values())
        {
            if(neco.getNazev().equals(hledanaVec))
            {
                nalezeno = neco;
                break;
            }
        }
        return nalezeno;
    }
    
    /**
     * Vrací název prostoru, který sousedí s aktuálním prostorem. Název sousední místnosti je zadán jako parametr.
     * V případě, že spolu zadané místnosti nesousedí, vrací hodnotu null.
     * 
     * @param nazevSousedniMistnosti - název vedlejšího pokoje
     * @return hledane mistnosti - seznam dostupných východů z aktuální místnosti
     */
    public Mistnost napisSousedniMistnost(String nazevSousedniMistnosti)
    {
        List<Mistnost> hledaneMistnosti = vychody.stream().filter(sousedni -> sousedni.getNazev().equals(nazevSousedniMistnosti))
        .collect(Collectors.toList()); 
        
        if(hledaneMistnosti.isEmpty())
        {
            return null;
        }
        else
        {
            return hledaneMistnosti.get(0);
        }
    }
    
    
    /**
     * Vrací kolekci, která obsahuje jednotlivé dostupné východy z místnosti.
     * Nelze jej upravovat - přidávat či ubírat východy.
     * 
     * @return nemodifikovatelná kolekce dostupných východů z místnosti.
     */
    public Collection<Mistnost> getVychody()
    {
        return Collections.unmodifiableCollection(vychody);
    }
    
    
    /**
     * Přidává věc do místnosti.
     * 
     * @param neco - vkládaná věc
     */
    public void pridejVec(Vec neco)
    {
        veci.put(neco.getNazev(), neco);
    }
    
    /**
     * Vrací věc, která se nachází v místnosti. 
     * 
     * @param nazev - název věci, kterou hledám
     * @return hledaná věc
     */
    public Vec vratVec(String nazev)
    {
        return veci.get(nazev);
    }
    
    /**
     * Vrací true nebo false, podle toho, jestli je zadaná věc v místnosti.
     * 
     * @param nazevVeci - název věci, o které zjišťuji, zda-li se v pokoji nachází
     * @return true - pokud je věc v místnosti jinak false
     */
    public boolean nachaziSeVMistnosti(String nazevVeci)
    {
        return veci.containsKey(nazevVeci);
    }
    
   /**
    * Vrací seznam věcí v místnosti.
    * 
    * @return seznamVeci - řetězec, který vypíše seznam věcí v místnosti.
    */
    private String seznamVeciVMistnosti()
    {
        String seznamVeci = "";
        if(veci.isEmpty())
        {
            seznamVeci = "Nic se zde nenachází.";
        }
        else
        {
            seznamVeci = "Nachází se zde: ";
            for(String neco : veci.keySet())
            {
                seznamVeci += neco + " ";
            }
        }
        return seznamVeci;
    }
}








