
package cz.vse.kolv.logika;
import java.util.Map;
import java.util.HashMap;

/*******************************************************************************
 * Třída Vec vytváří jednotlivé věci ve hře. Nastaví parametry, které je dále možné jednotlivým věcem přiřazovat 
 * v herním plánu. Např. trezor lze odemknout. 
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class Vec
{
    private String nazev;
    private boolean prenositelna;
    private boolean lzeOdemknout = false;
    private boolean odemcena = false;
    private boolean lzeRozriznout = false;
    private boolean jeRozriznuty = false;
    private boolean lzeSnist = false;
    private boolean jeSneden = false;
    private boolean lzeOtevrit = false;
    private boolean jeOtevrena = false;
    private boolean lzeProjit = false;
    private Map<String, Vec> obsah;
    
    /**
     * Konstruktor pro vytvoření věci, parametry název věci a zda-li se daná věc dá přenášet, tedy vkládat do batohu.
     * 
     * @param nazev - název věci, prenositelna - true, pokud věc lze sebrat, false, pokud věc sebrat nelze
     */
    public Vec(String nazev, boolean prenositelna)
    {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
        obsah = new HashMap<String, Vec>();
    }
    
    /**
     * Vrací název věci.
     */
    public String getNazev()
    {
        return nazev;
    }
    
    /**
     * Vrací, zda-li je věc nastavená jako přenositelná či nikoliv.
     * 
     * @return true, pokud věc lze sebrat a vložit tak do batohu, jinak false.
     */
    public boolean lzeSebrat()
    {
        return prenositelna;
    }
    
    /**
     * Nastaví věci schopnost, jestli lze odemknout. Využívá se u trezoru, kdy hráč nejprve musí trezor odemknout, aby mohl 
     * získat věci uvnitř. Trezor je jediná věc ve hře, která lze odemknout.
     * 
     * @param boolean lzeOdemknout - parametr je hodnota true, pokud lze věc odemknout, jinak false
     * 
     */
    public void setLzeOdemknout(boolean lzeOdemknout)
    {
        this.lzeOdemknout = lzeOdemknout;
    }
    
    /**
     * Vrací údaj, jestli věc lze odemknout.
     * 
     * @return true, pokud věc lze odemknout, jinak false.
     */
    public boolean jdeOdemknout()
    {
        return lzeOdemknout;
    }
    
    /**
     * Nastavuje u věci stav odemčení. Pokud hráč odemkne trezor, trezoru se přiřadí stav na odemčený.
     * Výchozí hodnota je nastavena na false, tudíž věc je zamčená a hráč ji nejprve musí odemknout.
     * 
     * @param hodnota true, pokud je věc odemčena, jinak false.
     */
    public void setJeOdemcena(boolean odemcena)
    {
        this.odemcena = odemcena;
    }
    
    /**
     * Vrací údaj o věci, jestli je odemčená či nikoliv.
     * 
     * @return true, pokud hráč věc odemkl, jinak false.
     */
    public boolean jeOdemcena()
    {
        return odemcena;
    }
    
    /**
     * Nastaví věci možnost, že ji lze rozříznout. Využívá se u plyšového medvídka, kterého je nejprve nutné 
     * rozříznout, aby hráč mohl získat věc, která se nachází uvnitř.
     * 
     * @param lzeRozriznout - hodnota true, pokud jde věc rozříznout, jinak false
     * 
     */
    public void setLzeRozriznout(boolean lzeRozriznout)
    {
        this.lzeRozriznout = lzeRozriznout;
    }
    
    /**
     * Vrací údaj o tom, zda-li lze věc rozříznout.
     * 
     * @return true, pokud věc jde rozříznout, jinak false.
     */
    public boolean jdeRozriznout()
    {
        return lzeRozriznout;
    }
    
    /**
     * Stejně jako u stavu odemykání, tato metoda při rozříznutí nastaví věci stav na rozříznutý.
     * 
     * @param jeRozriznuty - true, pokud byla věc rozříznuta, jinak false
     */
    public void setJeRozriznuty(boolean jeRozriznuty)
    {
        this.jeRozriznuty = jeRozriznuty;
    }
    
    /**
     * vrací údaj o tom, jestli byla věc rozříznuta. 
     * 
     * @return true, pokud hráč věc rozříznul, jinak false
     */
    public boolean jeRozriznuty()
    {
        return jeRozriznuty;
    }
    
    /**
     * Nastaví věci vlastnost, zda-li lze věc sníst. Využívá se u bonbonu, který lze sníst.
     * 
     * @param true, pokud lze věc sníst, false, pokud věc sníst nelze
     */
    public void setLzeSnist(boolean lzeSnist)
    {
        this.lzeSnist = lzeSnist;
    }
    
    /**
     * Vrací údaj, jestli věc lze sníst nebo ne.
     * 
     * @return true, pokud věc sníst lze, jinak false.
     */
    public boolean lzeSnist()
    {
        return lzeSnist;
    }
       
    /**
     * Nastavuje věci stav na snědená. Tedy pokud hráč věc snědl.
     * 
     * @param jeSneden - true, pokud hráč věc snědl, jinak false
     */
    public void setJeSneden(boolean jeSneden)
    {
        this.jeSneden = jeSneden;
    }
    
    /**
     * Vrací údaj o tom, zda-li byla věc snědena nebo ne.
     * 
     * @return true, pokud hráč věc snědl, jinak false.
     */
    public boolean jeSneden()
    {
        return jeSneden;
    }
    
    public void setLzeOtevrit(boolean lzeOtevrit)
    {
        this.lzeOtevrit = lzeOtevrit;
    }
    
    public boolean lzeOtevrit()
    {
        return lzeOtevrit;
    }
    
    public void setJeOtevrena(boolean jeOtevrena)
    {
        this.jeOtevrena = jeOtevrena;
    }
    
    public boolean jeOtevrena()
    {
        return jeOtevrena;
    }
    
    public void setLzeProjit(boolean lzeProjit)
    {
        this.lzeProjit = lzeProjit;
    }
    
    public boolean lzeProjit()
    {
        return lzeProjit;
    }
    
    
    
    /**
     * Metoda pro vložení věci do jiné věci. Např. hodinky jsou uloženy do trezoru.
     * Ověřuje, pokud daná věc lze odemknout nebo rozříznout, lze dovnitř vložit věc. 
     * Hráč poté musí věc nejprve odemknout nebo rozříznout, aby mohl věc uvnitř sebrat.
     * 
     * @param vec - vec, kterou vkládám dovnitř
     */
    public void vlozDovnitr (Vec vec)
    {
        if(this.lzeRozriznout || this.lzeOdemknout || this.lzeOtevrit())   
        {
            obsah.put(vec.getNazev(), vec);
        }
    }
    
    /**
     * Vrací údaj o tom, zda-li věc obsahuje nějakou jinou věc. Pokud hráč nějakou věc odemkne či rozřízne,
     * vrátí se, zda-li je nějaká věc uvnitř.
     * 
     * @param nazev - název věci
     * @return true, pokud je uvnitř věci jiná věc, jinak false
     */
    public boolean obsahuje(String nazev)
    {
        return (odemcena || jeRozriznuty || jeOtevrena) && obsah.containsKey(nazev);
    }
    
    /**
     * Vrací obsah věci. Pokud je uvnitř věci jiná věc, vrátí konkrétní věc, která se uvnitř nachází.
     * 
     * @return obsahVeci - obsah např. trezoru, co se nachází uvnitř.
     */
    public String vratObsah()
    {
        String obsahVeci = "";
        
        for(String neco : obsah.keySet())
        {
            obsahVeci += neco;
        }
        return obsahVeci;
    }
    
    /**
     * Metoda vytahuje věc z jiné věci. V případě, že daná věc byla odemčena či rozříznuta a zároveň se 
     * uvnitř nachází jiná věc a zároveň věc uvnitř lze sebrat, pak dojde k odebrání věci z jiné věci.
     * 
     * @param nazevVeci - název věci uvnitř jiné věci.
     * @return vyndana - věc, která se nachází uvnitř jiné věci
     * 
     */
    public Vec vytahni(String nazevVeci)
    {
        Vec vyndana = null;
        if(odemcena || jeRozriznuty || jeOtevrena && obsah.containsKey(nazevVeci))                
        {
            vyndana = obsah.get(nazevVeci);
            if(vyndana.lzeSebrat())
            {
                obsah.remove(nazevVeci);
            }
        }
        return vyndana;
    }
    
    /**
     * Metoda porovnává dvě různé věci. Dvě věci jsou shodné v případě, že mají shodný název.
     * 
     * 
     * @param o object, porovnává se s danou věcí.
     * @return true - pro dvě shodné věci, v jiném případě vrátí false
     */
    @Override
    public boolean equals(Object o)
    {
        if(this == o)
        {
           return true; 
        }    
        
        if(!(o instanceof Vec))
        {
            return false;
        }
        
        Vec dalsi = (Vec) o;
        return (java.util.Objects.equals(this.nazev, dalsi.getNazev()));
    }    
    
   /**
     * metoda hashCode vrací identifikátor instance ve formě čísla. Používá se pro
     * optimalizaci ukladani v dynamickych datových strukturách. Při překrytí metody equals
     * je nutné překrýt i metodu hashCode.
     */
    @Override
    public int hashCode()
    {
        int vysledek = 3;
        int hashNazvu = java.util.Objects.hashCode(this.nazev);
        vysledek = (hashNazvu + vysledek) * 37;
        return vysledek;
    }
}




