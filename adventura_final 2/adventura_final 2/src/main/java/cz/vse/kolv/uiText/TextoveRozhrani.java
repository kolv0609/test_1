
package cz.vse.kolv.uiText;

import java.util.Scanner;
import cz.vse.kolv.logika.IHra;

/*******************************************************************************
 * Instance třídy {@code TextoveRozhrani} představují ...
 * The {@code TextoveRozhrani} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class TextoveRozhrani
{
    private IHra hra;
    
    /***************************************************************************
     */
    public TextoveRozhrani(IHra hra)
    {
        this.hra = hra;
    }

    public void hraj()
    {
        System.out.println(hra.vratUvod());
        
        while (!hra.konecHry())
        {
            String radek = prectiString();
            System.out.println(hra.spustPrikaz(radek));
        }
        System.out.println(hra.vratZaver());
    }
    
    private String prectiString()
    {
        Scanner scanner = new Scanner(System.in);
        System.out.print("> ");
        return scanner.nextLine();
    }
}
