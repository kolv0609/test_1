
package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída PrikazSeber implementuje příkaz seber.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazSeber implements IPrikaz
{
   private static final String nazev = "seber";
   private HerniPlan plan;
   
   /**
    * Konstruktor pro vytvoření příkazu.
    * 
    * @param plan - herní plán
    */
    public PrikazSeber(HerniPlan plan)
    {
        this.plan = plan;
    }
   
    /**
     * Metoda provádí příkaz ve hře. Ověřuje, zda-li se zadaná věc nachází v aktuální místnosti, 
     * ve které se hráč nachází. Dále v případě, že věc lze sebrat, odebere ji z aktuální místnosti a přidá do batohu.
     * 
     * @param parametry - název věci, která se má sebrat.
     * @return 
     */
   @Override
   public String spustPrikaz(String... parametry)
   {
       String vyhodnoceni = "";
       if(parametry.length == 0)
       {
           vyhodnoceni = "Zadej název věci, kterou mám sebrat do batohu.";
       }
       else
       {
       String nazevVeci = parametry[0];
       Mistnost aktualniMistnost = plan.getAktualniMistnost();
                    
       if(aktualniMistnost.obsahujeVec(nazevVeci))
       {
           Vec pozadovanaVec = aktualniMistnost.odeberVec(nazevVeci);
           if(pozadovanaVec == null)
           {
               vyhodnoceni = nazevVeci + " nelze sebrat do batohu.";
           }
           else
           {
               boolean vlozeniDoBatohu = plan.getBatoh().pridejDoBatohu(pozadovanaVec);
               if(vlozeniDoBatohu)
               {
                   vyhodnoceni = nazevVeci + " bylo přidáno do batohu.";
               }
               else
               {
               plan.getAktualniMistnost().pridejVec(pozadovanaVec);
               vyhodnoceni = nazevVeci + " nelze vložit, batoh je plný!";
               }
           }
       }
       else
       {
           vyhodnoceni = nazevVeci + " se v místnosti nenachází.";
       }
    }
    return vyhodnoceni;
   }
   
   /**
    * Vrací název příkazu.
    * 
    * @reuturn název příkazu
    */
   @Override
   public String getNazev()
   {
       return nazev;
   }    
   
}
