
package cz.vse.kolv.logika;

/*******************************************************************************
 * Třída PrikazRozrizni implementuje příkaz pro rozříznutí věci. Využívá se u věci medvidek.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazRozrizni implements IPrikaz
{
    private static final String nazev = "rozrizni";
    private HerniPlan plan;

    /**
     * Vytvoří příkaz
     * 
     * @param - herní plán s místnostmi a věcmi
     */
    public PrikazRozrizni(HerniPlan plan)
    {
        this.plan = plan;
    }

    /**
     * Spustí příkaz s parametrem názvu věci, která se má rozříznout.
     * V případě, že hráčem zadaná věc se nachází v akuální místnosti, hráč má v batohu nuz a zadaná věc lze rozříznout,
     * věc se rozřízne a hráč tak získá přístup k věcem uvnitř. Využívá se u plyšového medvídka ve hře.
     * 
     * @param - hráčem zadaná věc, kterou chce rozříznout
     * @return Vrací řetězec v závislosti na tom, zda-li se zadanou věc podařilo rozříznout či nikoliv.
     * V případě, že ano, vrací se také věci, které se nachází uvnitř.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        String vyhodnoceni = "";
        if(parametry.length == 0)
        {
            vyhodnoceni = "Nezadal jsi, co mám rozříznout.";
        }
        else
        {
            String nazevMedvidka = parametry[0];
            Mistnost aktualni = plan.getAktualniMistnost();
            
            if(aktualni.obsahujeVec(nazevMedvidka))
            {
                Vec medvidek = aktualni.vratVec(nazevMedvidka);
                if(medvidek.jdeRozriznout())
                {
                    Batoh batoh = plan.getBatoh();
                    if(batoh.obsahujeVec("nuz"))
                    {
                        medvidek.setJeRozriznuty(true);
                        return "Rozřízl jsi plyšového medvídka. Uvnitř vidíš: " + medvidek.vratObsah();
                    }
                    else
                    {
                        vyhodnoceni = "Musíš nejprve najít a sebrat nůž.";
                    }
                }
                else
                {
                    vyhodnoceni = "Toto nelze rozříznout";
                }
            }
            else
            {
                vyhodnoceni = "Zadaná věc se v místnosti nenachází.";
            }
        }
        return vyhodnoceni;
    }
    
    /**
     * Vrací název příkazu.
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}
