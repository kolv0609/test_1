
package cz.vse.kolv.logika;

import cz.vse.kolv.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;


/*******************************************************************************
 * Testovací třída MistnostTest slouží ke komplexnímu otestování
 * třídy Mistnost.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class MistnostTest
{

    /***************************************************************************
     * Inicializace předcházející spuštění každého testu a připravující tzv.
     * přípravek (fixture), což je sada objektů, s nimiž budou testy pracovat.
     */
    @Before
    public void setUp()
    {
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /**************************************************************************
     *
     * Testuje průchodnost mezi místnostmi.
     */
    @Test
    public void testLzeJit()
    {
        Mistnost pokoj1 = new Mistnost("Předsíň",", vstupní místnost do domu");
        Mistnost pokoj2 = new Mistnost("Chodba",", ze které se vstupuje do dalších místností");
        
        pokoj1.setVychod(pokoj2);
        pokoj2.setVychod(pokoj1);
        assertEquals(pokoj2, pokoj1.napisSousedniMistnost("Chodba"));
        assertEquals(null, pokoj2.napisSousedniMistnost("Pokoj"));
        
    }

}
